<?php

namespace Tests\Feature;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class CommentTest extends TestCase
{
    use RefreshDatabase, InteractsWithExceptionHandling;

    private $user; 
    private $post;

    public function setUp(): void 
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->post = factory(Post::class)->create();
    }

    public function test_can_create_comment()
    {
        Passport::actingAs($this->user);

        $response = $this->json('POST', route('comments.store', $this->post->slug), [
            'body' => 'test comment'
        ]);

        $response->assertStatus(201);
        $this->assertCount(1, $this->post->comments()->get());
        $response->assertExactJson([
            'data' => [
                'body' => 'test comment',
                'commentable_type' => 'App\\Post',
                'commentable_id' => 1,
                'creator_id' => $this->user->id,
                'parent_id' => null,
                'updated_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString(),
                'id' => 1
            ]
        ]);
    }

    public function test_not_found_comments()
    {
        Passport::actingAs($this->user);

        $response = $this->json('POST', route('comments.store', 'test'), [
            'body' => 'test comment'
        ]);

        $response->assertStatus(404);
        $response->assertJsonStructure([
            'message',
            'exception'
        ]);
    }

    public function test_cannot_add_comment_invalid_data()
    {
        Passport::actingAs($this->user);

        $response = $this->json('POST', route('comments.store', 'test'), [

        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'body' => [
                    'The body field is required.'
                ]
            ]
        ]);
    }

    public function test_can_update_comment()
    {
        Passport::actingAs($this->user);
        $comment = factory(Comment::class)->create([
            'commentable_id' => $this->post->id
        ]);

        $response = $this->json('PATCH', route('comments.update', [$this->post->slug, $comment->id]), [
            'body' => 'new body'
        ]);

        $response->assertOk();

        $response->assertExactJson([
            'data' => [
                'id' => $comment->id,
                'body' => 'new body',
                'commentable_type' => 'App\\Post',
                'commentable_id' => $this->post->id,
                'creator_id' => $comment->creator_id,
                'parent_id' => null,
                'created_at' => $comment->created_at->toDateTimeString(),
                'updated_at' => now()->toDateTimeString()
            ]
        ]);
    }

    public function test_can_delete_comment()
    {
        Passport::actingAs($this->user);
        $comment = factory(Comment::class)->create([
            'commentable_id' => $this->post->id
        ]);

        $response = $this->json('DELETE', route('comments.destroy', [$this->post->slug, $comment->id]));

        $response->assertOk();

        $this->assertCount(0, $this->post->comments()->get());

    }

    public function test_can_get_comments_in_post()
    {
        Passport::actingAs($this->user);
        $comments = factory(Comment::class, 1)->create([
            'commentable_id' => $this->post->id
        ]);

        $response = $this->json('GET', route('comments.index', $this->post->slug));

        $response->assertOk();

        $response->assertJsonCount(1, 'data');
    }
}
