<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class PostTest extends TestCase
{
    use RefreshDatabase, InteractsWithExceptionHandling;

    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        Passport::actingAs($this->user);
    }

    public function test_can_get_all_post()
    {
        factory(Post::class)->create([
            'title' => 'new title',
            'user_id' => 1,
            'content' => 'content of the post',
            'created_at' => '2019-01-23 02:06:06',
            'updated_at' => '2019-01-23 02:13:26',
        ]);

        $response = $this->json('GET', route('posts.index'));

        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [
                        [
                            'id',
                            'user_id',
                            'title',
                            'slug',
                            'content',
                            'created_at',
                            'updated_at',
                            'deleted_at',
                        ]
                    ],
                    'links' => [
                        'first',
                        'last',
                        'prev',
                        'next',
                    ],
                    'meta' => [
                        'current_page',
                        'from',
                        'last_page',
                        'path',
                        'per_page',
                        'to',
                        'total',
                    ]
        ]);
    }

    public function test_can_get_post()
    {
        $this->withoutExceptionHandling();
        $post = factory(Post::class)->create([
            'title' => 'new title',
            'user_id' => 1,
            'content' => 'content of the post',
            'created_at' => '2019-01-23 02:06:06',
            'updated_at' => '2019-01-23 02:13:26',
        ]);

        $response = $this->json('GET', route('posts.show', $post->slug));

        $response->assertOk();
        $this->assertEquals('new title', $post->title);
    }

    public function test_can_create_post()
    {
        $response = $this->json('POST', route('posts.store'), [
            'title' => 'test title',
            'content' => 'test content',
        ]);

        $response->assertStatus(201);
        $this->assertCount(1, Post::all());
    }

    public function test_cannot_create_without_title()
    {
        $response = $this->json('POST', route('posts.store'), [
            'content' => 'test content',
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'title' =>
                    [
                        'The title field is required.'
                    ]
            ]
        ]);
        $this->assertCount(0, Post::all());
    }

    public function test_cannot_create_without_data()
    {
        $response = $this->json('POST', route('posts.store'), [
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'title' =>
                    [
                        'The title field is required.'
                    ],
                'content' =>
                [
                    'The content field is required.'
                ]
            ]
        ]);
        $this->assertCount(0, Post::all());
    }

    public function test_can_update_post()
    {
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id
        ]);

        $response = $this->json('PATCH', route('posts.update', $post->slug), [
            'title' => 'new title',
            'content' => 'new content',
            'image' => 'image'
        ]);
        
        $this->assertEquals('new title', $post->fresh()->title);
        $response->assertOk();
    }

    public function test_can_delete_post()
    {
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id
        ]);

        $response = $this->json('DELETE', route('posts.destroy', $post->id));
        
        $this->assertCount(0, Post::all());
        $response->assertOk();
    }
}
