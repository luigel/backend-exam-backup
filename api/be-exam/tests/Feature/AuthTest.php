<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;

class AuthTest extends TestCase
{
    use RefreshDatabase, InteractsWithExceptionHandling;


    public function setUp(): void {
        parent::setUp();
        \Artisan::call('migrate',['-vvv' => true]);
        \Artisan::call('passport:install',['-vvv' => true]);
        \Artisan::call('db:seed',['-vvv' => true]);
    }

    public function test_can_register()
    {
        $response = $this->json('POST', route('register'), [
            'name' => 'Juan Dela Cruz',
            'email' => 'juan_dela_cruz@ligph.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $response->assertStatus(201);
        $this->assertCount(1, User::all());
        $response->assertJsonStructure([
            'name',
            'email',
            'updated_at',
            'created_at',
            'id'
        ]);
    }

    public function test_cannot_register_invalid_data()
    {
        factory(User::class)->create([
            'email' => 'test@email.com'
        ]);

        $response = $this->json('POST', route('register'), [
            'name' => 'Rigel Kent Carbonel',
            'email' => 'test@email.com',
            'password' => 'password',
            'password_confirmation' => 'hello'
        ]);
        
        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'email' => [
                    'The email has already been taken.'
                ],
                'password' => [
                    'The password confirmation does not match.'
                ]
            ]
        ]);
    }

    public function test_cannot_register_no_name()
    {
        $response = $this->json('POST', route('register'), [
            'email' => 'test@email.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);
        
        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => [
                    'The name field is required.'
                ]
            ]
        ]);
    }

    public function test_can_login()
    {
        $user = factory(User::class)->create();

        $response = $this->json('POST', route('login'), [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $response->assertOk();
        $response->assertJsonStructure([
            'token',
            'token_type',
            'expires_at'
        ]);
    }

    public function test_cannot_login_invalid_data()
    {
        $user = factory(User::class)->create();

        $response = $this->json('POST', route('login'), [
            'email' => 'test@email.com',
            'password' => 'password'
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'email' => [
                    'These credentials do not match our records.'
                ]
            ]
        ]);
    }

    public function test_cannot_login_without_data()
    {
        $user = factory(User::class)->create();

        $response = $this->json('POST', route('login'), [

        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'email' => [
                    'The email field is required.'
                ],
                'password' => [
                    'The password field is required.'
                ]
            ]
        ]);
    }
    
    public function test_can_logout()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $response = $this->json('POST', route('logout'), []);

        $response->assertOk();
    }
}
