<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('posts', 'PostController')->except(['index', 'show']);
    // Route::apiResource('comments', 'CommentController')->except(['index', 'show']);
    Route::post('posts/{post}/comments', 'CommentController@store')->name('comments.store');
    Route::patch('posts/{post}/comments/{comment}', 'CommentController@update')->name('comments.update');
    Route::delete('posts/{post}/comments/{comment}', 'CommentController@destroy')->name('comments.destroy');

    Route::post('logoout', 'Auth\LogoutController')->name('logout');
});

Route::group(['middleware' => 'api'], function () {
    Route::apiResource('posts', 'PostController')->only(['index', 'show']);
    Route::get('/posts/{posts}/comments', 'CommentController@index')->name('comments.index');
    
    Route::post('/register', 'Auth\RegisterController')->name('register');

    Route::post('/login', 'Auth\LoginController')->name('login');
});