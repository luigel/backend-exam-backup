<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Post::class, function (Faker $faker) {
    $title = $faker->sentence;
    return [
        'title' => $title,
        'content' => $faker->paragraph,
        'image' => 'http://path/to/image',
        'slug' => Str::slug($title),
        'user_id' => function () {
            return factory(User::class)->create()->id;
        }
    ];
});
