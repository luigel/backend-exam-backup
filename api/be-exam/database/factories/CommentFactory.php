<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use App\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    $commentable = factory(Post::class)->create();
    $creator = factory(User::class)->create();
    return [
        'body' => $faker->paragraph,
        'commentable_type' => Post::class,
        'commentable_id' => $commentable->id,
        'creator_id' => $creator->id
    ];
});
