# LIG PH
## BE Exam
This is a REST API for the Backend Exam in LIG. 

### Installation
- Copy `.env-example` to `.env` and set the Database information in the new env
- Run `composer install` to install dependencies
- Migrate the migrations with `php artisan migrate`
- Run `php artisan passport:install` to create encryption keys needed.

### Run the test
    ./vendor/bin/phpunit
### Run the app
    php artisan serve
### Documentation 
You can check the documentation of the API here. 

### Packages Used
- Laravel Passport (https://github.com/laravel/passport)
- Laravel CORS (https://github.com/spatie/laravel-cors)

https://documenter.getpostman.com/view/78990/RznLHGgv?version=latest#f36672c9-6ccd-45d2-87cd-d3f101086cda


### NOTE
I used a package, `laravel-ide-helper` from barrydh. https://github.com/barryvdh/laravel-ide-helper.

Good luck to me :)
