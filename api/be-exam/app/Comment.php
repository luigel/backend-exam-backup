<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];

    protected $casts = [
        'commentable_id' => 'integer',
        'creator_id' => 'integer',
        'parent_id' => 'integer'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }
}
