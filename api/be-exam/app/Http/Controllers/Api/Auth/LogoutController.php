<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    public function __invoke(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        $token->delete();

        return response('', 200);
    }
}
