<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;

class LoginController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;

                return response()->json([
                    'token' => $token,
                    'token_type' => 'bearer',
                    'expires_at' => now()->addDay()
                ]);
            } 
            else {
                return $this->returnInvalid();
            } 
        }
        else {
            return $this->returnInvalid();
        }

    }

    private function returnInvalid()
    {
        return response()->json([
            'message' => 'The given data was invalid.',
            'errors' => [
                'email' => [
                    'These credentials do not match our records.'
                ]
            ]
        ], 422);
    }
}
