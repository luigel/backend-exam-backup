<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;

class RegisterController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    
        $request['password']=Hash::make($request['password']);
        $user = User::create($request->toArray());
 
        // $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        
        return response()->json([
            'name' => $user->name,
            'email' => $user->email,
            'updated_at' => $user->updated_at,
            'created_at' => $user->created_at,
            'id' => $user->id
        ], 201);
    }
}
