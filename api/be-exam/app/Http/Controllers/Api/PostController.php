<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Post;
use Illuminate\Http\Request;
use Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Resources\PostResource
     */
    public function index()
    {
        return PostResource::collection(Post::paginate());
    }

    /**
     * Store a newly created post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response|\App\Http\Resources\PostResource
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);

        $post = $request->user()->posts()->create([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'content' => $request->content,
            'image' => $request->image
        ]);

        return (new PostResource($post))->response()->setStatusCode(201);
    }

    /**
     * Display the specified post.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response|\App\Http\Resources\PostResource
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->first();

        if (!$post)
        {
            return response()->json([
                'message' => 'No query results for model [App\\Post].'
            ], 404);
        }

        return new PostResource($post);
    }

    /**
     * Update the specified post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \App\Http\Resources\PostResource
     */
    public function update(Request $request, $slug)
    {
        $post = $request->user()->posts()->where('slug', $slug)->first();
        $post->title = $request->title;
        $post->content = $request->content;
        $post->image = $request->image;
        $post->slug = Str::slug($request->title);
        
        $post->save();
        return (new PostResource($post->fresh()))->response()->setStatusCode(200);
    }

    /**
     * Remove the specified post.
     * 
     * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $request->user()->posts()->where('id', $id)->first()->delete();

        return response()->json([
            'status' => 'record deleted successfully'
        ]);
    }
}
