<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the comments in a post
     *
     * @return \App\Http\Resources\CommentResource
     */
    public function index($slug)
    {
        $post = Post::where('slug', $slug)->first();

        if (!$post)
        {
            return $this->returnResponseError();
        }

        return CommentResource::collection($post->comments()->paginate());
    }

    /**
     * Store a newly created comment in the post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param string $slug
     * @return \Illuminate\Http\Response|\App\Http\Resources\CommentResource
     */
    public function store(Request $request, $slug)
    {
        $request->validate([
            'body' => 'required|max:255'
        ]);
        
        $post = Post::where('slug', $slug)->first();

        if (!$post)
        {
            return $this->returnResponseError();
        }
        
        $comment = $post->comments()->create([
            'body' => $request->body,
            'creator_id' => $request->user()->id
        ]);

        return (new CommentResource($comment))->response()->setStatusCode(201);
    }


    /**
     * Update the specified comment in the post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $slug
     * @param int $id
     * @return \Illuminate\Http\Response|\App\Http\Resources\CommentResource
     */
    public function update(Request $request, $slug, $id)
    {
        $request->validate([
            'body' => 'required|max:255'
        ]);

        $post = Post::where('slug', $slug)->first();

        if (!$post)
        {
            return $this->returnResponseError();
        }

        $comment = $post->comments()->where('id', $id)->first();

        $comment->body = $request->body;
        $comment->save();

        return new CommentResource($comment->fresh());
    }

    /**
     * Remove the specified comment in the post.
     *
     * @param string $slug
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug, $id)
    {
        $post = Post::where('slug', $slug)->first();

        if (!$post)
        {
            return $this->returnResponseError();
        }

        $comment = $post->comments()->where('id', $id)->first();

        if ($comment)
        {
            $comment->delete();
        }

        return response()->json([
            'status' => 'record deleted successfully'
        ]);
    }

    private function returnResponseError()
    {
        return response()->json([
            'message' => 'No query results for model [BrianFaust\\Commentable\\Models\\Comment]',
            'exception' => 'Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException'
        ], 404);
    }
}
